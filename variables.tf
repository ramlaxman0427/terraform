variable "key_name" {
  description = "Name of the SSH keypair to use in AWS."
  default = "Terraformkey"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-west-2"
}

# ubuntu-trusty-14.04 (x64)
variable "aws_amis" {
  default = {
    "us-west-2" = "ami-07b4f3c02c7f83d59"
  }
}
